package main;

public class Main {

    private static final int EXCLUSIVE_TOP = 1000;

    public static void main(String[] args) {

        System.out.printf("Sum of all multiples of 3 and 5 below %d:%n%d",
                EXCLUSIVE_TOP,
                sumMultiples(EXCLUSIVE_TOP,
                        3, 5
                )
        );

    }

    private static int sumMultiples(int top, int... multiples) {
        int sum = 0;
        for (int i = 1; i < top; i++){
            if (checkMultiplication(i, multiples)){
                sum += i;
            }
        }
        return sum;
    }

    private static boolean checkMultiplication(int i, int[] multiples) {
        for (int multiple : multiples){
            if (i % multiple == 0)
                return true;
        }
        return false;
    }

}
